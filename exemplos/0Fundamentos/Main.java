import java.util.Scanner;

/**
 * Main
 */
public class Main {
    public static void main(String[] args) {
        int idade = 20;
        double altura = 1.75;
        boolean temCarteiraMotorista = true;
        char sexo = 'M';
        String nome = "João";

        System.out.println(idade);
        System.out.println(altura);
        System.out.println(temCarteiraMotorista);
        System.out.println(sexo);
        System.out.println(nome);

        idade = 25; // Redefine a idade.
        int idadeDaquiACincoAnos = idade + 5;
        System.out.println("Daqui a cinco anos, você terá " + idadeDaquiACincoAnos + " anos.");

        double salario = 1500.75;
        double salarioAumentado = salario * 1.1;
        System.out.println("Seu novo salário é: " + salarioAumentado);

        boolean isAprovado = true;
        System.out.println("Você está aprovado? " + isAprovado);

        char letra = 'A';
        System.out.println("A letra é: " + letra);

        Scanner scanner = new Scanner(System.in);
        nome = scanner.nextLine();
        System.out.println("Olá, " + nome);

        scanner.close();
    }
}