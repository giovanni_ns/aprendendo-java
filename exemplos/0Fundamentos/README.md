# Fundamentos de Programação em Java

Vamos nos familiarizar com alguns conceitos básicos de programação usando a linguagem Java.

## Variáveis e tipos de dados

Em Java, as variáveis são usadas para armazenar dados. Cada variável tem um tipo de dado associado, que define o tipo de valor que ela pode armazenar. Vamos dar uma olhada nos tipos de dados básicos em Java:

+ int: usado para armazenar números inteiros.
+ double: usado para armazenar números com ponto flutuante.
+ boolean: usado para armazenar valores verdadeiros ou falsos.
+ char: usado para armazenar caracteres individuais.
+ String: usado para armazenar cadeias de caracteres.

``` java
int idade;
double salario;
boolean isAprovado;
char letra;
String nome;
```

+ A variável `idade` é do tipo `int` e pode armazenar números inteiros, como 25 ou -10.
+ A variável `salario` é do tipo `double` e pode armazenar números com ponto flutuante, como 1500.75 ou -99.99.
+ A variável `isAprovado` é do tipo `boolean` e pode armazenar os valores verdadeiro (`true`) ou falso (`false`).
+ A variável `letra` é do tipo `char` e pode armazenar um único caractere, como 'A', 'b' ou '?'.
+ A variável `nome` é do tipo `String` e pode armazenar cadeias de caracteres, como "João" ou "Maria"

## Atribuição de valores

Depois de declarar uma variável, você pode atribuir um valor a ela usando o operador de atribuição (`=`). Vejamos exemplos de atribuição de valores:

``` java
idade = 25;
salario = 1500.75;
isAprovado = true;
letra = 'A';
nome = "João";
```

## Utilizando as variáveis

Depois de atribuir valores às variáveis, você pode usá-las em expressões ou exibi-las na saída.

``` java
int idade = 25;
int idadeDaquiACincoAnos = idade + 5;
System.out.println("Daqui a cinco anos, você terá " + idadeDaquiACincoAnos + " anos.");

double salario = 1500.75;
double salarioAumentado = salario * 1.1;
System.out.println("Seu novo salário é: " + salarioAumentado);

boolean isAprovado = true;
System.out.println("Você está aprovado? " + isAprovado);

char letra = 'A';
System.out.println("A letra é: " + letra);

String nome = "João";
System.out.println("Olá, " + nome);
```

Nesses exemplos, realizamos operações com as variáveis e as utilizamos para exibir mensagens personalizadas na saída.

---

## Classe Scanner

A classe `Scanner` é uma classe da biblioteca padrão do Java que permite ler entrada do usuário a partir do console. Ela fornece métodos para ler diferentes tipos de dados, como números inteiros, números de ponto flutuante, caracteres e strings.

Para começar a usar a classe `Scanner`, siga os passos abaixo:

1. No início do seu código Java, você precisa importar a classe `Scanner`. Adicione a seguinte linha antes da declaração da classe:

    ``` java
    import java.util.Scanner;
    ```

2. Após a declaração da classe, crie uma instância da classe `Scanner` especificando a fonte de entrada, neste caso, `System.in`, que representa a entrada padrão (console). Você pode nomear a instância como quiser. Veja o exemplo abaixo:

    ``` java
    Scanner scanner = new Scanner(System.in);
    ```

3. Agora você pode usar os métodos da instância de `Scanner` para ler diferentes tipos de dados.

    Aqui estão alguns dos métodos mais comuns:

    + `nextLine()`: lê uma linha inteira de texto (cadeia de caracteres) digitada pelo usuário.
    + `nextInt()`: lê um número inteiro digitado pelo usuário.
    + `nextDouble()`: lê um número de ponto flutuante (`double`) digitado pelo usuário.
    + `nextBoolean()`: lê um valor booleano (`true` ou `false`) digitado pelo usuário.
    + `next()` ou `nextToken()`: lê a próxima sequência de caracteres separada por espaços ou quebra de linha.

    Exemplo de uso da classe `Scanner`:

    ``` java
    import java.util.Scanner;

    public class ExemploScanner {
        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);

            System.out.print("Digite seu nome: ");
            String nome = scanner.nextLine();

            System.out.print("Digite sua idade: ");
            int idade = scanner.nextInt();

            System.out.println("Nome: " + nome);
            System.out.println("Idade: " + idade);

            scanner.close();
        }
    }
    ```

    Neste exemplo, criamos uma instância da classe `Scanner` chamada `scanner` que lê dados do console (`System.in`). Usamos o método `nextLine()` para ler uma linha de texto (o nome) e o método `nextInt()` para ler um número inteiro (a idade). Em seguida, exibimos os valores lidos na saída.

    Certifique-se de chamar o método `close()` no objeto `Scanner` para liberar os recursos associados a ele quando você terminar de usá-lo.
