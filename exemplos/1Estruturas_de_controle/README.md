# Sintaxe e Estrutura da Linguagem Java

Vamos explorar a sintaxe e a estrutura básica da linguagem Java. Veremos como definir classes, criar objetos, declarar variáveis e métodos, e utilizar estruturas de controle.

## Definição de Classe e Criação de Objetos

Em Java, a estrutura básica é a classe. Uma classe é um modelo para criar objetos. Vamos começar com um exemplo simples de uma classe chamada "Carro":

``` java
public class Carro {
    // Atributos
    String marca;
    String modelo;
    int ano;

    // Método principal
    public static void main(String[] args) {
        // Criação de objetos
        Carro meuCarro = new Carro();
        meuCarro.marca = "Toyota";
        meuCarro.modelo = "Corolla";
        meuCarro.ano = 2021;

        System.out.println("Meu carro é um " + meuCarro.marca + " " + meuCarro.modelo + " do ano " + meuCarro.ano + ".");
    }
}
```

Neste exemplo, definimos a classe "Carro" com três atributos: "marca", "modelo" e "ano". Em seguida, no método `main`, criamos um objeto chamado `meuCarro` e definimos os valores dos atributos. Por fim, imprimimos as informações do carro.

## Estruturas de Controle

As estruturas de controle são usadas para controlar o fluxo de execução do programa. Em Java, temos estruturas de controle condicionais (if, else if, else) e de repetição (for, while, do-while). Veja um exemplo:

``` java
    int idade = 18;

    if (idade >= 18) {
        System.out.println("Maior de idade");
    } else {
        System.out.println("Menor de idade");
    }

    int contador = 0;
    while (contador < 5) {
        System.out.println("Contador: " + contador);
        contador++;
    }
```

Neste exemplo, verificamos se a variável "idade" é maior ou igual a 18 e imprimimos uma mensagem correspondente. Em seguida, utilizamos um loop while para imprimir o valor do contador de 0 a 4.

Neste exemplo, utilizaremos um loop for para imprimir os números de 1 a 5. A estrutura do loop for é composta por três partes: a inicialização (`int i = 1`), a condição de continuação (`i <= 5`), e a atualização (`i++`). O bloco de código dentro do loop é executado repetidamente enquanto a condição de continuação for verdadeira.

``` java
 for (int i = 1; i <= 5; i++) {
            System.out.println(i);
        }
```
