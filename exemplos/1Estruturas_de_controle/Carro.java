public class Carro {
    // Atributos
    String marca;
    String modelo;
    int ano;

    // Método principal
    public static void main(String[] args) {
        // Criação de objetos
        Carro meuCarro = new Carro();
        meuCarro.marca = "Toyota";
        meuCarro.modelo = "Corolla";
        meuCarro.ano = 2021;

        System.out.println("Meu carro é um " + meuCarro.marca + " " + meuCarro.modelo + " do ano " + meuCarro.ano + ".");
    }
}
