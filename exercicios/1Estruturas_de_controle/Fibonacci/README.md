# Exercício

Escreva um programa em Java que utilize um loop for para imprimir a sequência de Fibonacci até o décimo termo.

A sequência de Fibonacci é uma sequência de números em que cada número é a soma dos dois números anteriores. A sequência começa com 0 e 1.

Dica: Você pode utilizar duas variáveis para armazenar os números anteriores e atualizar seus valores a cada iteração do loop.

Sequência de Fibonacci. Inicializamos as variáveis `primeiroTermo` e `segundoTermo` com os valores iniciais da sequência. Dentro do loop, imprimimos o `primeiroTermo` atual e calculamos o próximo termo somando `primeiroTermo` e `segundoTermo`. Em seguida, atualizamos os valores das variáveis `primeiroTermo` e `segundoTermo` para a próxima iteração.

Resultado será:

    Sequência de Fibonacci até o décimo termo:
    0 1 1 2 3 5 8 13 21 34
