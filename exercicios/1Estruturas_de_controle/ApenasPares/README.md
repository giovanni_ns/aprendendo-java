# Exercício

Agora, aqui está um exercício para você praticar o uso do loop for:

Escreva um programa em Java que utilize um loop for para imprimir a soma dos números pares de 1 a 10.

Você pode utilizar a estrutura do loop for e a operação módulo (`%`) para verificar se um número é par ou não.

Dica: Você pode utilizar uma variável acumuladora para armazenar a soma dos números pares.

Dentro do loop, verificamos se o número é par usando a operação módulo (`i % 2 == 0`) e, caso seja par, adicionamos o número à variável soma.
No final, imprimimos o resultado da soma dos números pares.

Resultado será:

    A soma dos números pares de 1 a 10 é: 30
