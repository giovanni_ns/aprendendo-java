# Exercício

Escreva um programa em Java que declare uma variável para armazenar a temperatura atual em graus Celsius, atribua um valor a essa variável e, em seguida, converta a temperatura para Fahrenheit usando a fórmula: `F = (C * 9/5) + 32`. Exiba a temperatura em Celsius e Fahrenheit na saída.

Exemplo de saída esperada:

    Temperatura em Celsius: 28.5
    Temperatura em Fahrenheit: 83.3
