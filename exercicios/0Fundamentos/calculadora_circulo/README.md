# Exercício

Escreva um programa em Java que solicite ao usuário o raio de um círculo e calcule a área e o perímetro desse círculo. Use as fórmulas: `área = π * raio²` e `perímetro = 2 * π * raio`. Considere `π` como 3.14159. Exiba os resultados na saída.

    Digite o raio do círculo: 5
    Área do círculo: 78.53975
    Perímetro do círculo: 31.4159
