# Aprendendo Java

Este repositório contém exemplos de código e recursos para ajudá-lo a aprender Java. Aqui, você encontrará uma variedade
de tópicos e exercícios para fortalecer suas habilidades de programação em Java.

## Estrutura do Repositório

O repositório está organizado da seguinte forma:

+ `exemplos/`: Esta pasta contém exemplos de código abordando diferentes conceitos de programação em Java. Cada exemplo
  é fornecido com explicações claras e comentários úteis para facilitar o entendimento.

+ `exercicios/`: Nesta pasta, você encontrará exercícios práticos para aplicar e aprimorar seu conhecimento em Java. Os
  exercícios variam em nível de dificuldade e cobrem várias áreas da linguagem.

+ `recursos/`: Aqui você encontrará recursos adicionais, como links para tutoriais, documentação oficial do Java e
  outros materiais de aprendizagem úteis.

## Como usar este repositório

1. Clone o repositório para o seu ambiente local usando o seguinte comando:

``` bash
git clone https://gitlab.com/giovanni_ns/aprendendo-java.git
```

2. Navegue pelas pastas `exemplos/` e `exercicios/` para explorar o código fonte fornecido. Abra os arquivos em um ambiente
   de desenvolvimento Java de sua escolha para visualizar e estudar o código.

3. Se você estiver procurando por exercícios práticos, consulte a pasta exercicios/. Cada exercício está em sua própria
   pasta com instruções detalhadas dentro do arquivo README.md. Leia as instruções cuidadosamente e tente resolver os
   exercícios por conta própria.

4. A pasta `recursos/` contém links úteis para tutoriais, documentação oficial do Java e outros recursos online para expandir
seus conhecimentos em Java. Explore esses recursos e utilize-os como referência durante seu aprendizado.

## Contribuição

Se você quiser contribuir para este repositório, sinta-se à vontade para fazer um fork, fazer as alterações desejadas e
enviar um pull request. Suas contribuições são bem-vindas e ajudarão a melhorar o conteúdo para outros aprendizes de
Java.

## Aviso Legal

Este repositório é apenas para fins educacionais e destina-se a auxiliar no aprendizado de programação em Java. Os
exemplos de código fornecidos aqui são de natureza educacional e podem não seguir as melhores práticas de produção.
Recursos Adicionais

Aqui estão alguns recursos adicionais que podem ser úteis enquanto você aprende Java:

+ [Documentação oficial do Java](https://docs.oracle.com/en/java/)
+ [Tutorial de Java da W3Schools](https://www.w3schools.com/java/)
+ [Java Tutorials (Oracle)](https://docs.oracle.com/javase/tutorial/index.html)
+ [Java Programming (Coursera)](https://www.coursera.org/specializations/java-programming)
+ [Java - The Complete Reference (Livro)](https://www.amazon.com/Java-Complete-Reference-Herbert-Schildt/dp/1260440230)
